FROM debian:9 as build

RUN apt update && apt install -y wget gcc make
RUN  wget https://github.com/PCRE2Project/pcre2/releases/download/pcre2-10.40/pcre2-10.40.tar.gz  && tar xvfz pcre2-10.40.tar.gz  -C /usr/bin
RUN wget https://zlib.net/zlib-1.2.12.tar.gz && tar xvfz zlib-1.2.12.tar.gz -C /usr/bin
RUN wget https://nginx.org/download/nginx-1.22.0.tar.gz &&  tar xvfz nginx-1.22.0.tar.gz &&  cd nginx-1.22.0 && ./configure --with-pcre=/usr/bin/pcre2-10.40 --with-zlib=/usr/bin/zlib-1.2.12 && make && make install

FROM debian:9
WORKDIR /usr/local/nginx/sbin
COPY --from=build /usr/local/nginx/sbin/nginx .
COPY --from=build /usr/bin/pcre2-10.40 /usr/bin/pcre2-10.40
COPY --from=build /usr/bin/zlib-1.2.12 /usr/bin/zlib-1.2.12
RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx
CMD ["./nginx", "-g", "daemon off;"]